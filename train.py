from agent.agent import Agent
from functions import *
import sys
import matplotlib.pyplot as plt

from keras.callbacks import TensorBoard, EarlyStopping

def plotGraph(profits, trades):

	fig = plt.figure(figsize=(14,7), facecolor='white')
	ax = fig.add_subplot(111)
	ax.plot(profits, label='profit')
	ax.plot(trades, label='count trade')
	plt.title(stock_name + " profit each episode" + str(episode_count) + " ep "+str(window_size)+"window.png")
	plt.grid(ls='--')
	plt.legend(loc='upper left', frameon=True)
	# plt.show()
	plt.savefig("images/"+stock_name+"profit"+str(episode_count)+"ep"+str(window_size)+"window.png", format='png', bbox_inches='tight', transparent=True)

try:
	if len(sys.argv) != 4:
		print ("Usage: python train.py [stock] [window] [episodes]")
		exit()

	stock_name, window_size, episode_count = sys.argv[1], int(sys.argv[2]), int(sys.argv[3])

	agent = Agent(window_size)
	data = getStockDataVec(stock_name)
	l = len(data) - 1
	batch_size = 32

	profits = []
	trades = []

	prev_total_profit = -1000000

	for e in range(episode_count + 1):
		print ("Episode " + str(e) + "/" + str(episode_count))
		state = getState(data, 0, window_size + 1)

		total_profit = 0
		agent.inventory = []
		count_trade = 0
		
		for t in range(l):
			action = agent.act(state)

			# sit
			next_state = getState(data, t + 1, window_size + 1)
			reward = 0

			if action == 1: # buy
				agent.inventory.append(data[t])
				# print ("Buy: " + formatPrice(data[t]))
				count_trade+=1

			elif action == 2 and len(agent.inventory) > 0: # sell
				bought_price = agent.inventory.pop(0)
				reward = max(data[t] - bought_price, 0)
				total_profit += data[t] - bought_price
				# print ("Sell: " + formatPrice(data[t]) + " | Profit: " + formatPrice(data[t] - bought_price))
				count_trade+=1

			done = True if t == l - 1 else False
			agent.memory.append((state, action, reward, next_state, done))
			state = next_state

			if done:
				print ("--------------------------------")
				print ("Total Profit: " + formatPrice(total_profit)+ " count_trade: "+ str(count_trade))
				print ("--------------------------------")

				profits.append(total_profit)
				trades.append(count_trade)

			if len(agent.memory) > batch_size:
				agent.expReplay(batch_size)

		print("prev_total_profit", prev_total_profit)

		if e % 10 == 0:
			agent.model.save("models/" + stock_name + "ep" + str(e) + "window" + str(window_size))
		if total_profit > prev_total_profit:
			agent.model.save("models_plus/" + stock_name + "ep" + str(e) + "window" + str(window_size))
			prev_total_profit = total_profit

	plotGraph(profits, trades)
except Exception as e:
	print("Error occured: {0}".format(e))
finally:
	exit()