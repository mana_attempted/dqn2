import keras
from keras.models import load_model

from agent.agent import Agent
from functions import *
import sys
import numpy as np

import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook

def plotGraph(price, buy_actions, sell_actions, model, profit, count_trade):

	fig = plt.figure(figsize=(14,7), facecolor='white')
	ax = fig.add_subplot(111)
	ax.plot(price, label='price')
	ax.plot(buy_actions, "g.", label='buy')
	ax.plot(sell_actions, "r.", label='sell')
	plt.title(stock_name+" "+model + " profit: "+ profit +" count_trade: " + str(count_trade))
	plt.grid(ls='--')
	plt.legend(loc='upper left', frameon=True)
	# plt.show()
	plt.savefig("images/"+stock_name+""+model+".png", format='png', bbox_inches='tight', transparent=True)

try:
	if len(sys.argv) != 3:
		print ("Usage: python evaluate.py [stock] [model]")
		exit()

	stock_name, model_name = sys.argv[1], sys.argv[2]
	model = load_model("models/" + model_name)
	window_size = model.layers[0].input.shape.as_list()[1]

	agent = Agent(window_size, True, model_name)
	data = getStockDataVec(stock_name)
	# data = normalize(data)
	l = len(data) - 1
	batch_size = 32

	state = getState(data, 0, window_size + 1)
	total_profit = 0
	agent.inventory = []

	# Setup our plot
	fig, ax = plt.subplots()
	timeseries_iter = 0
	plt_data = []

	buy_actions = []
	sell_actions = []

	count_trade = 0

	for t in range(l):
		action = agent.act(state)

		# sit
		next_state = getState(data, t + 1, window_size + 1)
		reward = 0
		

		if action == 1: # buy
			agent.inventory.append(data[t])
			plt_data.append((timeseries_iter, data[t], 'Buy'))
			print ("Buy: " + formatPrice(data[t]))

			buy_actions.append(data[t])
			sell_actions.append(None)

			count_trade+=1

		elif action == 2 and len(agent.inventory) > 0: # sell
			bought_price = agent.inventory.pop(0)
			reward = max(data[t] - bought_price, 0)
			total_profit += data[t] - bought_price
			plt_data.append((timeseries_iter, data[t], 'Sell'))
			
			print ("Sell: " + formatPrice(data[t]) + " | Profit: " + formatPrice(data[t] - bought_price))

			buy_actions.append(None)
			sell_actions.append(data[t])

			count_trade+=1

		else:
			buy_actions.append(None)
			sell_actions.append(None)

		timeseries_iter += 1
		done = True if t == l - 1 else False
		agent.memory.append((state, action, reward, next_state, done))
		state = next_state

		if done:
			print ("--------------------------------")
			print (stock_name + " Total Profit: " + formatPrice(total_profit)+ " count_trade: "+ str(count_trade))
			print ("--------------------------------")
		
		if len(agent.memory) > batch_size:
				agent.expReplay(batch_size) 

	# plt_data = np.array(plt_data)
	# ax.plot(plt_data[:, 0])
	# ax.plot(plt_data[:, 1])
	# Display our plots
	# plt.grid(ls='--')
	# plt.legend(loc='upper left', frameon=True)
	# plt.show()

	plotGraph(data, buy_actions, sell_actions, model_name, formatPrice(total_profit), count_trade)

except Exception as e:
	print("Error is: " + e)
finally:
	exit()
